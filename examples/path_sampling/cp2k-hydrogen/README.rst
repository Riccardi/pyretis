
Dissociation of Hydrogen with CP2K
==================================

This is an example of using PyRETIS together with CP2K.
More information about this example can be found on the
PyRETIS web-site.

Before you run this example, please copy the files:

1. ``BASIS_SET``
2. ``GTH_POTENTIALS``

from your CP2K distribution and place them in the ``cp2k_input`` folder.
