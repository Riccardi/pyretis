# -*- coding: utf-8 -*-
# Copyright (c) 2022, PyRETIS Development Team.
# Distributed under the LGPLv2.1+ License. See LICENSE for more info.
"""This package defines common methods which are used for testing.

Package structure
-----------------

Modules
~~~~~~~

compare.py (:py:mod:`pyretis.testing.compare`)
    Common methods for comparing results.

helpers.py (:py:mod:`pyretis.testing.helpers`)
    Common methods for tests.

"""
